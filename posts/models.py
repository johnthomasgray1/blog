from django.db import models
from django.conf import settings

class Tag(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Joke(models.Model):
    prompt = models.CharField(max_length=250)
    punchline = models.TextField()
    score = models.IntegerField(default=1)
    vote = models.BooleanField()
    posted_on = models.DateField(null=True, blank = True)
    tags = models.ManyToManyField(Tag)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name = "jokes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.prompt
