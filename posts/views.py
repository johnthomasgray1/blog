from django.shortcuts import render, redirect
from jokes.models import Joke
from django.contrib.auth.decorators import login_required
from jokes.forms import JokeForm, JokeFilterForm
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models import Count, Q


def list_jokes(request):
    jokes = Joke.objects.all()
    tag_filter_form = JokeFilterForm(request.GET)

    if tag_filter_form.is_valid():
        selected_tags = tag_filter_form.cleaned_data.get('tags')
        if selected_tags:
            # Annotate each post with the count of matched tags
            jokes = jokes.annotate(matched_tag_count=Count('tags', filter=Q(tags__in=selected_tags)))

            # Filter out jokes that don't have all of the selected tags
            jokes = jokes.filter(matched_tag_count=len(selected_tags)).distinct()

    context = {
        "jokes" : jokes,
        "tag_filter_form" : tag_filter_form,
    }
    return render(request, "jokes/list.html", context)

@login_required(login_url="login")
def create_joke(request):
    if request.method == "POST":
        form = JokeForm(request.POST)
        if form.is_valid():
            joke = form.save(commit=False)
            joke.author = request.user
            joke.posted_on = timezone.now()
            joke.save()
            # change to post detail later
            return redirect("list_jokes")
    else:
        form = JokeForm()
    context = {
        "form" : form,
    }
    return render(request, "jokes/create.html", context)

def show_joke(request, id):
    joke = Joke.objects(id=id)
    context = {
        "joke" : joke,
    }
    return render(request, "jokes/show.html", context)
