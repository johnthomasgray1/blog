from django.urls import path
from posts.views import list_jokes, create_joke

urlpatterns = [
    path("", list_jokes, name="list_jokes"),
    path("create/", create_joke, name="create_joke"),
]
