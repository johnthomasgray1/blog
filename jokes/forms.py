from django import forms
from posts.models import Joke, Tag


class JokeForm(forms.ModelForm):
    class Meta():
        model = Joke
        exclude = [
            "author",
            "posted_on",
        ]

class JokeFilterForm(forms.Form):
    tags = forms.ModelMultipleChoiceField(
        queryset=Tag.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )
