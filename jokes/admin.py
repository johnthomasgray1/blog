from django.contrib import admin
from jokes.models import Joke, Tag


@admin.register(Joke)
class JokeAdmin(admin.ModelAdmin):
    list_display = [
        "prompt",
        "author",
        "punchline",
        "posted_on",
        "score",
        "vote",
    ]

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = [
        "name",
    ]
